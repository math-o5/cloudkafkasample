require('dotenv').config();
const Twit = require('twit');

// Setup keys
const clientTwit = new Twit({
    consumer_key:         process.env.TWITTER_CONSUMER_KEY,
    consumer_secret:      process.env.TWITTER_CONSUMER_SECRET,
    access_token:         process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret:  process.env.TWITTER_ACCESS_TOKEN_SECRET,
    timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
    strictSSL:            true,     // optional - requires SSL certificates to be valid.
});

// Search params used
let search_params = { q: 'JavaScript', count: 10  };

/*
    Extract text from tweets responses.
*/
function filterData(err, data, response) {
    let tweets = data.statuses;
    for (let i = 0; i < tweets.length; i++) {
        console.log(tweets[i].text);
    }
}

/**
 * Search tweets on stream.
 */
function retrieveData() {
    clientTwit.get('search/tweets', search_params, filterData);
}

// Once every hour
setInterval(retrieveData, 60*60*1000);



