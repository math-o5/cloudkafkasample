const avro = require('avro-js');

let type = avro.parse({
    name: 'Tweet',
    type: 'record',
    fields: [
      {name: 'text', type: 'string'}
    ]
});